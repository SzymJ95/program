﻿using System;
using System.Drawing;
using System.IO;
using System.Windows.Forms;
using Npgsql;

namespace Program
{
    public partial class Form1 : Form
    {
        int licznik;
        int lastLineNumber;
        public static string sLogowanie;
        public static string identyfikator;
        public static string dostawa;

        public Form1()
        {
            InitializeComponent();
            NextLine();
            OpenConn();
        }


        private void NextLine()
        {
            using (StreamReader file = new System.IO.StreamReader(@"C:\SZY\Login.SZY"))
            {
                for (licznik = 0; licznik < lastLineNumber; licznik++)
                    file.ReadLine();

                string tbHost = file.ReadLine();
                lastLineNumber++;
                string tbPort = file.ReadLine();
                lastLineNumber++;
                string tbUser = file.ReadLine();
                lastLineNumber++;
                string tbPass = file.ReadLine();
                lastLineNumber++;
                string tbDataBaseName = file.ReadLine();
                lastLineNumber++;
                identyfikator = file.ReadLine();
                lastLineNumber++;
                sLogowanie = string.Format("Server={0};Port={1};" + "User Id={2};Password={3};Database={4};", tbHost, tbPort, tbUser, tbPass, tbDataBaseName);
            }
        }

        private void OpenConn()
        {
            try
            {
                string sOpenConn = "SELECT imienazwisko FROM login WHERE identyfikator = '" + identyfikator + "';";
                NpgsqlConnection conn = new NpgsqlConnection(sLogowanie);
                NpgsqlCommand openConn = new NpgsqlCommand(sOpenConn, conn);
                conn.Open();
                openConn.ExecuteNonQuery();
                identyfikator = Convert.ToString(openConn.ExecuteScalar());
                if (identyfikator != "" && (identyfikator != null))
                {
                    textBox1.Text = identyfikator;
                    conn.Close();
                }
                else
                {
                    conn.Close();
                    MessageBox.Show("Twój klucz identyfikatora jest źle wpisany.");
                    Environment.Exit(0);
                }
            }
            catch (Exception)
            {
                MessageBox.Show("Server jest offline, spróbuj ponownie później.");
                Environment.Exit(0);
            }
        }

        private void Dostawy()
        {
            string sDostawy = "SELECT data FROM dostawy WHERE data = '" + dostawa + "';";
            NpgsqlConnection conn = new NpgsqlConnection(sLogowanie);
            NpgsqlCommand openConn = new NpgsqlCommand(sDostawy, conn);
            conn.Open();
            openConn.ExecuteNonQuery();
            dostawa = Convert.ToString(openConn.ExecuteScalar());
            if (dostawa != "" && (dostawa != null))
            {
                textBox2.BackColor = Color.Lime;
            }
            else
            {
                textBox2.BackColor = Color.White;
            }
            conn.Close();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (dostawa != "" && (dostawa !=null))
            {
                 Form laptopy = new Form2();
                 laptopy.Show();
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (dostawa != "" && (dostawa !=null))
            {
                //Form komputery = new Form3();
                //komputery.Show();
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {
            textBox2.Text = DateTime.Now.ToShortDateString();
            dostawa = textBox2.Text;
            Dostawy();
        }

        private void textBox2_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                dostawa = textBox2.Text;
                Dostawy();
            }
        }
    }
}