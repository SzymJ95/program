﻿using Npgsql;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace Program
{
    public partial class Form2 : Form
    {
        string serial;
        char[] cutter = { ' ', '!', '#', '$', '%', '^', '&', '*', '(', ')', '-', '_', '=', '+', '@' };
        NpgsqlConnection conn = new NpgsqlConnection(Form1.sLogowanie);

        public Form2()
        {
            InitializeComponent();
            Count();
        }

        private void Count()
        {
            textBox99.Text = Form1.identyfikator;
            textBox97.Text = Form1.dostawa;
            string sCount = "SELECT COUNT(*) FROM laptopy WHERE dostawa = '" + Form1.dostawa + "';";
            NpgsqlCommand count = new NpgsqlCommand(sCount, conn);
            conn.Open();
            count.ExecuteNonQuery();
            Int32 iCount = Convert.ToInt32(count.ExecuteScalar());
            label15.Text = iCount.ToString();
            conn.Close();
        }

        private void Parameters()
        {
            NpgsqlCommand cmd = new NpgsqlCommand("INSERT INTO laptopy (serial, marka, model, cale, procesor, zegar, ram, ddr, dysk, typ, naped, grade, uwagi, kto, dostawa, datadodania, czas) values(:serial, :marka, :model, :cale, :procesor, :zegar, :ram, :ddr, :dysk, :typ, :naped, :grade, :uwagi, :kto, :dostawa, :datadodania, :czas)", conn);
            cmd.Parameters.Add(new NpgsqlParameter("serial", textBox98.Text));
            cmd.Parameters.Add(new NpgsqlParameter("marka", textBox2.Text));
            cmd.Parameters.Add(new NpgsqlParameter("model", textBox3.Text));
            cmd.Parameters.Add(new NpgsqlParameter("cale", textBox4.Text));
            cmd.Parameters.Add(new NpgsqlParameter("procesor", textBox5.Text));
            cmd.Parameters.Add(new NpgsqlParameter("zegar", textBox6.Text));
            cmd.Parameters.Add(new NpgsqlParameter("ram", textBox7.Text));
            cmd.Parameters.Add(new NpgsqlParameter("ddr", textBox8.Text));
            cmd.Parameters.Add(new NpgsqlParameter("dysk", textBox9.Text));
            cmd.Parameters.Add(new NpgsqlParameter("typ", textBox10.Text));
            cmd.Parameters.Add(new NpgsqlParameter("naped", textBox11.Text));
            cmd.Parameters.Add(new NpgsqlParameter("grade", textBox12.Text));
            cmd.Parameters.Add(new NpgsqlParameter("uwagi", textBox13.Text));
            cmd.Parameters.Add(new NpgsqlParameter("kto", Form1.identyfikator));
            cmd.Parameters.Add(new NpgsqlParameter("dostawa", Form1.dostawa));
            cmd.Parameters.Add(new NpgsqlParameter("datadodania", DateTime.Now.ToShortDateString()));
            cmd.Parameters.Add(new NpgsqlParameter("czas", DateTime.Now.ToString("HH:mm:ss")));
            conn.Open();
            cmd.ExecuteNonQuery();
            conn.Close();
            Count();
        }

        private void textBox_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                serial = textBox1.Text.Trim(cutter);
                textBox1.Clear();
                textBox98.Text = serial;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            string tymczasowa;
            if (checkBox1.Checked)
            {
                textBox1.BackColor = Color.White;
                textBox98.BackColor = Color.White;
                textBox2.BackColor = Color.White;
                textBox3.BackColor = Color.White;
                textBox4.BackColor = Color.White;
                textBox5.BackColor = Color.White;
                textBox6.BackColor = Color.White;
                textBox7.BackColor = Color.White;
                textBox8.BackColor = Color.White;
                textBox9.BackColor = Color.White;
                textBox10.BackColor = Color.White;
                textBox11.BackColor = Color.White;
                textBox12.BackColor = Color.White;
                if (textBox98.Text != null && (textBox98.Text != ""))
                {
                    tymczasowa = textBox2.Text.Trim(cutter);
                    textBox2.Text = tymczasowa;
                    textBox2.BackColor = Color.White;
                    if (textBox2.Text != null && (textBox2.Text != ""))
                    {
                        tymczasowa = textBox3.Text.Trim(cutter);
                        textBox3.Text = tymczasowa;
                        textBox3.BackColor = Color.White;
                        if (textBox3.Text != null && (textBox3.Text != ""))
                        {
                            tymczasowa = textBox4.Text.Trim(cutter);
                            textBox4.Text = tymczasowa;
                            textBox4.BackColor = Color.White;
                            if (textBox4.Text != null && (textBox4.Text != ""))
                            {
                                tymczasowa = textBox5.Text.Trim(cutter);
                                textBox5.Text = tymczasowa;
                                textBox5.BackColor = Color.White;
                                if (textBox5.Text != null && (textBox5.Text != ""))
                                {
                                    tymczasowa = textBox6.Text.Trim(cutter);
                                    textBox6.Text = tymczasowa;
                                    textBox6.BackColor = Color.White;
                                    if (textBox6.Text != null && (textBox6.Text != ""))
                                    {
                                        tymczasowa = textBox7.Text.Trim(cutter);
                                        textBox7.Text = tymczasowa;
                                        textBox7.BackColor = Color.White;
                                        if (textBox7.Text != null && (textBox7.Text != ""))
                                        {
                                            tymczasowa = textBox8.Text.Trim(cutter);
                                            textBox8.Text = tymczasowa;
                                            textBox8.BackColor = Color.White;
                                            if (textBox8.Text != null && (textBox8.Text != ""))
                                            {
                                                tymczasowa = textBox9.Text.Trim(cutter);
                                                textBox9.Text = tymczasowa;
                                                textBox9.BackColor = Color.White;
                                                if (textBox9.Text != null && (textBox9.Text != ""))
                                                {
                                                    tymczasowa = textBox10.Text.Trim(cutter);
                                                    textBox10.Text = tymczasowa;
                                                    textBox10.BackColor = Color.White;
                                                    if (textBox10.Text != null && (textBox10.Text != ""))
                                                    {
                                                        tymczasowa = textBox11.Text.Trim(cutter);
                                                        textBox11.Text = tymczasowa;
                                                        textBox11.BackColor = Color.White;
                                                        if (textBox11.Text != null && (textBox11.Text != ""))
                                                        {
                                                            tymczasowa = textBox12.Text.Trim(cutter);
                                                            textBox12.Text = tymczasowa;
                                                            textBox12.BackColor = Color.White;
                                                            if (textBox12.Text != null && (textBox12.Text != ""))
                                                            {
                                                                DialogResult = MessageBox.Show("Czy chcesz wprowadzić towar? Twoje zmienne zostaną zapisane.", "Wprowadzanie nowego towaru.", MessageBoxButtons.YesNo);
                                                                if (DialogResult == DialogResult.Yes)
                                                                {
                                                                    Parameters();
                                                                }
                                                            }
                                                            else
                                                            {
                                                                MessageBox.Show("Wprowadź Grade.", "Czegoś Ci brakuje.");
                                                                textBox12.BackColor = Color.Red;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            MessageBox.Show("Sprawdź czy jest napęd.", "Czegoś Ci brakuje.");
                                                            textBox11.BackColor = Color.Red;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        MessageBox.Show("Wprowadź typ dysku.", "Czegoś Ci brakuje.");
                                                        textBox10.BackColor = Color.Red;
                                                    }
                                                }
                                                else
                                                {
                                                    MessageBox.Show("Wprowadź wielkość dysku.", "Czegoś Ci brakuje.");
                                                    textBox9.BackColor = Color.Red;
                                                }
                                            }
                                            else
                                            {
                                                MessageBox.Show("Wprowadź DDR.", "Czegoś Ci brakuje.");
                                                textBox8.BackColor = Color.Red;
                                            }
                                        }
                                        else
                                        {
                                            MessageBox.Show("Wprowadź RAM.", "Czegoś Ci brakuje.");
                                            textBox7.BackColor = Color.Red;
                                        }
                                    }
                                    else
                                    {
                                        MessageBox.Show("Wprowadź Zegar procesora.", "Czegoś Ci brakuje.");
                                        textBox6.BackColor = Color.Red;
                                    }
                                }
                                else
                                {
                                    MessageBox.Show("Wprowadź Procesor.", "Czegoś Ci brakuje.");
                                    textBox5.BackColor = Color.Red;
                                }
                            }
                            else
                            {
                                MessageBox.Show("Wprowadź Cale.", "Czegoś Ci brakuje.");
                                textBox4.BackColor = Color.Red;
                            }
                        }
                        else
                        {
                            MessageBox.Show("Wprowadź Model.", "Czegoś Ci brakuje.");
                            textBox3.BackColor = Color.Red;
                        }
                    }
                    else
                    {
                        MessageBox.Show("Wprowadź Marke.", "Czegoś Ci brakuje.");
                        textBox2.BackColor = Color.Red;
                    }
                }
                else
                {
                    MessageBox.Show("Wprowadź Serial number.", "Czegoś Ci brakuje.");
                    textBox1.BackColor = Color.Red;
                    textBox98.BackColor = Color.Red;
                }
            }
            else
            {
                textBox1.BackColor = Color.White;
                textBox98.BackColor = Color.White;
                textBox2.BackColor = Color.White;
                textBox3.BackColor = Color.White;
                textBox4.BackColor = Color.White;
                textBox5.BackColor = Color.White;
                textBox6.BackColor = Color.White;
                textBox7.BackColor = Color.White;
                textBox8.BackColor = Color.White;
                textBox9.BackColor = Color.White;
                textBox10.BackColor = Color.White;
                textBox11.BackColor = Color.White;
                textBox12.BackColor = Color.White;
                if (textBox98.Text != null && (textBox98.Text != ""))
                {
                    tymczasowa = textBox2.Text.Trim(cutter);
                    textBox2.Text = tymczasowa;
                    textBox2.BackColor = Color.White;
                    if (textBox2.Text != null && (textBox2.Text != ""))
                    {
                        tymczasowa = textBox3.Text.Trim(cutter);
                        textBox3.Text = tymczasowa;
                        textBox3.BackColor = Color.White;
                        if (textBox3.Text != null && (textBox3.Text != ""))
                        {
                            tymczasowa = textBox4.Text.Trim(cutter);
                            textBox4.Text = tymczasowa;
                            textBox4.BackColor = Color.White;
                            if (textBox4.Text != null && (textBox4.Text != ""))
                            {
                                tymczasowa = textBox5.Text.Trim(cutter);
                                textBox5.Text = tymczasowa;
                                textBox5.BackColor = Color.White;
                                if (textBox5.Text != null && (textBox5.Text != ""))
                                {
                                    tymczasowa = textBox6.Text.Trim(cutter);
                                    textBox6.Text = tymczasowa;
                                    textBox6.BackColor = Color.White;
                                    if (textBox6.Text != null && (textBox6.Text != ""))
                                    {
                                        tymczasowa = textBox7.Text.Trim(cutter);
                                        textBox7.Text = tymczasowa;
                                        textBox7.BackColor = Color.White;
                                        if (textBox7.Text != null && (textBox7.Text != ""))
                                        {
                                            tymczasowa = textBox8.Text.Trim(cutter);
                                            textBox8.Text = tymczasowa;
                                            textBox8.BackColor = Color.White;
                                            if (textBox8.Text != null && (textBox8.Text != ""))
                                            {
                                                tymczasowa = textBox9.Text.Trim(cutter);
                                                textBox9.Text = tymczasowa;
                                                textBox9.BackColor = Color.White;
                                                if (textBox9.Text != null && (textBox9.Text != ""))
                                                {
                                                    tymczasowa = textBox10.Text.Trim(cutter);
                                                    textBox10.Text = tymczasowa;
                                                    textBox10.BackColor = Color.White;
                                                    if (textBox10.Text != null && (textBox10.Text != ""))
                                                    {
                                                        tymczasowa = textBox11.Text.Trim(cutter);
                                                        textBox11.Text = tymczasowa;
                                                        textBox11.BackColor = Color.White;
                                                        if (textBox11.Text != null && (textBox11.Text != ""))
                                                        {
                                                            tymczasowa = textBox12.Text.Trim(cutter);
                                                            textBox12.Text = tymczasowa;
                                                            textBox12.BackColor = Color.White;
                                                            if (textBox12.Text != null && (textBox12.Text != ""))
                                                            {
                                                                DialogResult = MessageBox.Show("Czy chcesz wprowadzić towar?", "Wprowadzanie nowego towaru.", MessageBoxButtons.YesNo);
                                                                if (DialogResult == DialogResult.Yes)
                                                                {
                                                                    Parameters();
                                                                    button2.PerformClick();
                                                                }
                                                            }
                                                            else
                                                            {
                                                                MessageBox.Show("Wprowadź Grade.", "Czegoś Ci brakuje.");
                                                                textBox12.BackColor = Color.Red;
                                                            }
                                                        }
                                                        else
                                                        {
                                                            MessageBox.Show("Sprawdź czy jest napęd.", "Czegoś Ci brakuje.");
                                                            textBox11.BackColor = Color.Red;
                                                        }
                                                    }
                                                    else
                                                    {
                                                        MessageBox.Show("Wprowadź typ dysku.", "Czegoś Ci brakuje.");
                                                        textBox10.BackColor = Color.Red;
                                                    }
                                                }
                                                else
                                                {
                                                    MessageBox.Show("Wprowadź wielkość dysku.", "Czegoś Ci brakuje.");
                                                    textBox9.BackColor = Color.Red;
                                                }
                                            }
                                            else
                                            {
                                                MessageBox.Show("Wprowadź DDR.", "Czegoś Ci brakuje.");
                                                textBox8.BackColor = Color.Red;
                                            }
                                        }
                                        else
                                        {
                                            MessageBox.Show("Wprowadź RAM.", "Czegoś Ci brakuje.");
                                            textBox7.BackColor = Color.Red;
                                        }
                                    }
                                    else
                                    {
                                        MessageBox.Show("Wprowadź Zegar procesora.", "Czegoś Ci brakuje.");
                                        textBox6.BackColor = Color.Red;
                                    }
                                }
                                else
                                {
                                    MessageBox.Show("Wprowadź Procesor.", "Czegoś Ci brakuje.");
                                    textBox5.BackColor = Color.Red;
                                }
                            }
                            else
                            {
                                MessageBox.Show("Wprowadź Cale.", "Czegoś Ci brakuje.");
                                textBox4.BackColor = Color.Red;
                            }
                        }
                        else
                        {
                            MessageBox.Show("Wprowadź Model.", "Czegoś Ci brakuje.");
                            textBox3.BackColor = Color.Red;
                        }
                    }
                    else
                    {
                        MessageBox.Show("Wprowadź Marke.", "Czegoś Ci brakuje.");
                        textBox2.BackColor = Color.Red;
                    }
                }
                else
                {
                    MessageBox.Show("Wprowadź Serial number.", "Czegoś Ci brakuje.");
                    textBox1.BackColor = Color.Red;
                    textBox98.BackColor = Color.Red;
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            textBox98.Clear();
            textBox99.BackColor = Color.White;
            textBox2.Clear();
            textBox2.BackColor = Color.White;
            textBox3.Clear();
            textBox3.BackColor = Color.White;
            textBox4.Clear();
            textBox4.BackColor = Color.White;
            textBox5.Clear();
            textBox5.BackColor = Color.White;
            textBox6.Clear();
            textBox6.BackColor = Color.White;
            textBox7.Clear();
            textBox7.BackColor = Color.White;
            textBox8.Clear();
            textBox8.BackColor = Color.White;
            textBox9.Clear();
            textBox9.BackColor = Color.White;
            textBox10.Clear();
            textBox10.BackColor = Color.White;
            textBox11.Clear();
            textBox11.BackColor = Color.White;
            textBox12.Clear();
            textBox12.BackColor = Color.White;
            textBox13.Clear();
        }
    }
}